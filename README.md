## Project Start

The goal of this project is to create, read, understand, and customize code using a specific imported package. Creating a game is something Ive always wanted to do, so for this project it seemed like a natural fit. This project takes form as the old 2D game Galaga. The code is written in python and uses the imported files of PyGame. This will be done via working with some existing code and the wiki for the package pygame. From a learning stand point this project is about understanding how to aquire and use coding external packages in order to achieve a goal. Much like we used Vader to study english literature. This project uses Pygame to create a game. When starting this project I found it hard to know where to start. Reading code for similar games which use pyGame helped me understand the tools that I have avalible to me. When starting this project my goals were having working code which visually plays the game galaga, and to add as many layers of customization as possible depending on difficutly. Addtions such as sound, custom images, and levels. The finished project features a space ship which shoots soccer balls at an infinite number of Nick Cage heads falling from the sky. As each wave passes the level increase in the top left corner. The code for sound effects is written any ready to be incorperated, however the files they draw upon require a specific formate. Understandinf the different pieces of code and how they interact with each other is a massive challenge, and each level of detail added, no matter how small results in an extreme amount of work and complexity. The project has tought me that proper documentation of code is a nessiesity to a customizable/updatable code. It is easy to forget what vital segments of the code perpuse is, making it even easier to break a working code without realizing it. I really enjoyed working with a visual interface, where I could run additional lines of code and the program would still run, but would crash at the time in which the code is called upon. For example, when I added the audio sound effects to the collision system, the syntax was correct so the code would run. However, at the moment of a collision event python would crash. This was because the code could not open the file in question when it was ask to open it. This was an interesting and different method of fixing bugs in the code, which I really enjoyed. I can see why games require so much experiemental debuging in there development. Even when the code runs, it doesnt mean that there arent still bugs. 


# Install: 
-This program requires pygame to run.


-The coded file must be within the same file as the images, and they must have the correct name according to there png callouts in the code.

-I have not run the juypter file, it was purely used for documentation purposes.

-The .py file is the one I exacuted, however it is extremely non-insightful.

-A large amount of the time spend on this project was messing around with different segments of the code and discovering how they influenced the code

-This is discribed in the juypter file.

- Running the code is not vital: see screen shot to view the interface and game in action (The ship shoots soccer balls)

Wiki for PyGame: https://www.pygame.org/wiki/
Related Code: https://github.com/dgparker/ship-game
 
 